﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Internal_Assessment
{
    public class Employee
    {
        public int EmpNo { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public double Salary { get; set; }
        public double Commission { get; set; }
        public int DeptNo { get; set; }

        public void ShowDetails()
        {
            Console.WriteLine("EmpNo = {0}, Name = {1}, Designation = {2}, Salary = {3}, Commission = {4}, DeptNo = {5}", EmpNo.ToString(), Name, Designation, Salary.ToString(), Commission.ToString(), DeptNo.ToString());
        }

        public double Calculate_Total_Salary()
        {
            return (Salary + Commission);
        }
    }
}
