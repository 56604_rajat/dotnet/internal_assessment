﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Internal_Assessment
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<object> list = new List<object>();
            bool status = true;
            while (status)
            {
                Console.WriteLine("Enter your choice 1.Add Employee Details\n 2.Add Department Details\n 3.Show Employee Details\n 4.Show Department Details\n 5.Total salary of all Employees\n 6.Display all employees of particualar department\n 7.Department wise employee count\n 8.Department wise average salary\n 9.department wise minimum salary\n 10.Exit");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Employee employee = new Employee();
                        Console.WriteLine("Enter Details");
                        Console.WriteLine("Employee No");
                        int empno = Convert.ToInt32(Console.ReadLine());
                        employee.EmpNo = empno;
                        Console.WriteLine("Name");
                        string name = Console.ReadLine();
                        employee.Name = name;
                        Console.WriteLine("Designation");
                        string designation = Console.ReadLine();
                        employee.Designation = designation;
                        Console.WriteLine("Salary");
                        double salary = Convert.ToDouble(Console.ReadLine());
                        employee.Salary = salary;
                        Console.WriteLine("Commission");
                        double commission = Convert.ToDouble(Console.ReadLine());
                        employee.Commission = commission;
                        Console.WriteLine("DeptNo");
                        int deptno = Convert.ToInt32(Console.ReadLine());
                        employee.DeptNo = deptno;

                        list.Add(employee);
                        Console.WriteLine("Added Successfully");
                        break;
                    case 2:
                        Department department = new Department();
                        Console.WriteLine("Enter Details");
                        Console.WriteLine("Department No");
                        int _deptno = Convert.ToInt32(Console.ReadLine());
                        department.DeptNo = _deptno;
                        Console.WriteLine("Department Name");
                        string deptname = Console.ReadLine();
                        department.DeptName = deptname;
                        Console.WriteLine("Location");
                        string location = Console.ReadLine();
                        department.Location = location;

                        list.Add(department);
                        Console.WriteLine("Added Successfully");
                        break;
                    case 3:
                        for (int i = 0; i < list.Count; i++)
                        {
                            object type = list[i];
                            if (type is Employee)
                            {
                                Employee emp = (Employee)type;
                                emp.ShowDetails();
                            }
                        }
                        break;
                    case 4:
                        for (int i = 0; i < list.Count; i++)
                        {
                            object type = list[i];
                            if (type is Department)
                            {
                                Department dept = (Department)type;
                                dept.ShowDetails();
                            }
                        }
                        break;
                    case 5:
                        for (int i = 0; i < list.Count; i++)
                        {
                            object type = list[i];
                            if (type is Employee)
                            {
                                Employee emp = (Employee)type;
                                Console.WriteLine("No = {0}, Name = {1}, Total salary = {2}", emp.EmpNo.ToString(), emp.Name, emp.Calculate_Total_Salary().ToString());
                            }
                        }
                        break;
                    case 6:
                        Console.WriteLine("Enter Dept No");
                        int depno = Convert.ToInt32(Console.ReadLine());
                        for (int i = 0; i < list.Count; i++)
                        {
                            object type = list[i];
                            if (type is Employee)
                            {
                                Employee emp = (Employee)type;
                                if (emp.DeptNo == depno)
                                {
                                    emp.ShowDetails();
                                }
                            }
                        }
                        break;
                    case 7:
                        int count = 0;
                        Console.WriteLine("Enter Dept No");
                        int dno = Convert.ToInt32(Console.ReadLine());
                        for (int i = 0; i < list.Count; i++)
                        {
                            object type = list[i];
                            if (type is Employee)
                            {
                                Employee emp = (Employee)type;
                                if (emp.DeptNo == dno)
                                {
                                    count = count + 1;
                                }
                            }
                        }
                        Console.WriteLine("Department no {0} has {1} employees", dno.ToString(), count.ToString());
                        break;
                    case 8:
                        int count1 = 0;
                        double sum = 0;
                        Console.WriteLine("Enter Dept No");
                        dno = Convert.ToInt32(Console.ReadLine());
                        for (int i = 0; i < list.Count; i++)
                        {
                            object type = list[i];
                            if (type is Employee)
                            {
                                Employee emp = (Employee)type;
                                if (emp.DeptNo == dno)
                                {
                                    sum = sum + emp.Salary;
                                    count1 = count1 + 1;
                                }
                            }
                        }
                        double average = sum / count1;
                        Console.WriteLine("Average salary of department No {0} is Rs.{1}", dno.ToString(), average.ToString());
                        break;
                    case 9:
                        double min = 0;
                        int a = 0;
                        Console.WriteLine("Enter Dept No");
                        dno = Convert.ToInt32(Console.ReadLine());
                        for (int i = 0; i < list.Count; i++)
                        {
                            object type = list[i];
                            if (type is Employee)
                            {
                                Employee emp = (Employee)type;
                                if (emp.DeptNo == dno)
                                {
                                    a = a + 1;
                                    if (a == 1)
                                    {
                                        min = emp.Salary;
                                    }

                                    if (min > emp.Salary)
                                    {
                                        min = emp.Salary;
                                    }
                                }
                            }
                        }
                        Console.WriteLine("Department No {0} has minimum salary of Rs.{1}", dno.ToString(), min.ToString());
                        break;
                    case 10:
                        status = false;
                        break;
                    default:
                        Console.WriteLine("Enter valid option");
                        break;
                }
            }
            Console.ReadLine();
        }
    }
}
