﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Internal_Assessment
{
    public class Department
    {
        public int DeptNo { get; set; }
        public string DeptName { get; set; }
        public string Location { get; set; }
        public void ShowDetails()
        {
            Console.WriteLine("Department No = {0}, Department Name = {1}, Location = {2}", DeptNo.ToString(), DeptName, Location);
        }
    }
}
